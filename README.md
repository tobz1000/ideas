# Ideas

Ideas for stuff I probably won't do

## Note taker

- Repo: https://gitlab.com/tobz1000/jot-vscode
- File-watcher daemon which auto-syncs/saves to git repo
- Quick search of deleted content in version history
- Ability to purge content from history

## Media player

- Functional queries for librarys and playlists
- History of now-playing lists w/ saved progress

## Puzzle game

- Challenge: Use building blocks to cause certain outcomes with specified inputs
- Each completed level provides a new block with which to solve further levels
- Physically based, e.g. coloured balls rolling down rails

## Tactics game

- Grid-based, turn-based tactical combat
- Player input is based around match setup rather than turn-by-turn instructions
- Unit upgrade points must be split between traditional upgrades (health, damage etc) and strategies that the unit can employ
- Simple control flows available for player to specify/"program"
- Fog of war: e.g. a mortar might require a scout unit to spot first

## Language

- Repo: https://gitlab.com/tobz1000/stateful-script
- Types as values with variable-time evaluation
- Effects/states described with linear types
- Imperative-feeling data manipulation over immutable values

## Ergonomic Rust codegen framework

- Repo: https://gitlab.com/tobz1000/comptime-rs
- Goals:
  - Allow procedural macros to be defined within a consuming crate/module
  - Allow definitions to be terse enough to be defined ad-hoc
  - An alternative to `syn`'s syntax types which are more semantic-based than syntax-based, allowing
  for better compile-time checks, and sacrificing some freedom of output style (e.g. `use` blocks
  are automatically derived)
- Requires an additional pre-processing mechanism

## Mock HTTP server for unit tests: record & save responses from real server on first run

## Rust partial struct hack

See partial-struct-hack.md

## ML audio processing: remove spoken speech from music

## DRG Mod: Loadout/outfit customizer

- Save sets of settings/costume items which can be combined to create a complete loadout
- Import loadout sets from karl.gg

There's this: https://mod.io/g/drg/m/equipment-presets

## Rust "isolated modules"

- Declare modules within a crate to be compiled separately, to improve check/build performance
- Isolated modules can't reference the base crate. Multiple isolated modules within a crate can reference each other.
  - If references create circular dependencies, add a "notice"-level log to compilation, and combine the compilation unit for all modules in the chain.

### Technical details
- Compile the base crate and isolated mods as separate crates.
- Symlink relevant src dirs if possible within some staging folder
- Create manifests for isolated module crates: just copy the actual crate's manifest for the most part. Name them something like `__isolated_mod_[module-path]`
- Copy the crate's manifest to staging folder and add isolated mod deps

Specify an isolated mod within its parent like:

```rust
#[isolated_mod] bar;
```

Expands to roughly:

```rust
mod bar {
  pub use __isolated_mod_[module-path]::*;
}
```

Still to figure out:
- Macros are exported at crate root, so their paths would change; can we fix this?
- How to efficiently convert import paths within isolated modules, e.g. `crate::bar::baz` should become `crate::baz`, where `bar` is the isolated mod