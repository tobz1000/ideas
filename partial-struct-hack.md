# Rust partial struct hack

```rs
fn main() {
    let partial1 = partial! {
        // User must specify name of partial-ised type. If we want to let user specify name of
        // target type instead, then get partial via a trait associated type, would required GATs.
        PartialSt { 
            a: 1,
            b: 2
        }
    };
    let partial2 = partial! {
        PartialSt {
            b: 3,
            c: "Four".to_string()
        }
    };
    let partial3 = partial1.merge(partial2); // Has all fields
    let full: St = partial.into(); // Compile-time checked conversion
    let partial = PartialSt { a: (), b: 4, c: () };
    // let partial = <S as PartialStruct>::Partial { a: (), b: 4, c: () };
}

struct St {
    a: usize,
    b: usize,
    c: String,
}

trait SelectField<A, B> {
    type Selected;

    fn select(a: A, b: B) -> Self::Selected;
}

impl SelectField<(), usize> for S {
    type Selected = usize;

    fn select(a: (), b: usize) -> Self::Selected {
        b
    }
}

impl SelectField<usize, ()> for St {
    type Selected = usize;

    fn select(a: usize, b: ()) -> Self::Selected {
        a
    }
}

impl<A> SelectField<(), ()> for A {
    type Selected = ();

    fn select(a: (), b: ()) -> Self::Selected {
        ()
    }
}

struct PartialSt<A = (), B = (), C = ()> {
    a: A,
    b: B,
    c: C,
}

impl Default for PartialSt<(), (), ()> {
    fn default() -> Self {
        PartialSt {
            a: (),
            b: (),
            c: ()
        }
    }
}

impl<A, B, C> PartialSt<A, B, C> {
    fn merge<D, E, F>(
        self,
        other: PartialSt<D, E, F>,
    ) -> PartialSt<
        <S as SelectField<A, D>>::Selected,
        <S as SelectField<B, E>>::Selected,
        <S as SelectField<C, F>>::Selected,
    >
    where
        St: SelectField<A, D> + SelectField<B, E> + SelectField<C, F>,
    {
        PartialSt {
            a: St::select(self.a, other.a),
            b: St::select(self.b, other.b),
            c: St::select(self.c, other.c),
        }
    }
}

impl From<PartialSt<usize, usize, String>> for St { ... }

impl de

macro_rules! partial_struct {
    ... => {
        $partial_struct_name {
            $fields...
            .. PartialSt::default()
        }
    }
    // Can use FRU syntax to allow user to only specify the populated fields - but requires
    // implementation of: https://github.com/rust-lang/rfcs/pull/2528
}
```

Will need additional trait constraints to ensure that `PartialSt` can only be constructed where `A`
is either `usize` or `()`, etc.